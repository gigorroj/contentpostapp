#!/usr/bin/env python3

import webapp

class contentApp(webapp.webApp):

    content = {'/': 'Root Page', '/javier': 'Javier Page', '/J': 'J Page'}

    def parse (self, request):
        metodo = request.split(' ', 2)[0]
        resource = request.split(' ',2)[1]
        body = request.split('\r\n\r\n', 2)[1]
        return (metodo, resource, body)

    def process (self, parsedRequest):
        """Process the relevant elements of the request.

        Returns the HTTP code for the reply, and an HTML page.
        """
        (metodo, resource, body) = parsedRequest
        if metodo == "POST":
            self.content[resource] = body
        if resource in self.content:
            httpcode = "200 Ok"
            htmlbody = ("<html><body><h1>El contenido de esta pagina es: <br>" + self.content[resource] +
                        ("<br>Si quieres modificarlo: <form action=" + resource + " method= post><input type= text name=content required><br><input type=submit value= Enviar></form></h1></body></html>"))
        else:
            httpcode = "400 Not Found"
            htmlbody = "<html><body><h1>NOT FOUND! Incluyelo tu mismo: "
            htmlbody += "<form action=" + resource + " method= post><input type= text name=content required><br><input type=submit value= Enviar></form></h1></body></html>"

        return (httpcode,htmlbody)



if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
